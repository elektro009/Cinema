package com.example.cinema001;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.example.cinema001.MainActivity;
import com.example.cinema001.MainCinemaActivity;
import com.example.cinema001.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {
	
	private String[] user;
	private String amount = "0";
	private String url = "http://192.168.147.140/cinema/android_php/";

	private EditText Username;
	private EditText Password;
	//private Button Register;
	private Button Login;
	private Button Register;
	/*
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putString(USERNAME, username);
	}
	
	*/
	//-------------------------LISTENER--------------------------------------------------
	
	// Login Listener
	private OnClickListener LoginListener = new OnClickListener() {
		

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			StrictMode.enableDefaults();
			SaveSharedPreferenceDatabase_data.setURL(MainActivity.this, url);
		        
			if(read_user(Username.getText().toString(),Password.getText().toString()) == true){
				
				SaveSharedPreferenceDatabase_data.setUser(MainActivity.this, Username.getText().toString(), Password.getText().toString(), amount);
				startMainCinemaActivity();
				Username.setText("");
				Password.setText("");
			}
			else
			{
				Toast.makeText(getBaseContext(), "Incorrect: Username or/and Password!", Toast.LENGTH_SHORT).show();
			}
		}

	};
	//start MainCinemaActivity
	private void startMainCinemaActivity()
	{
		Intent intent = new Intent(MainActivity.this, MainCinemaActivity.class);
		startActivity(intent);
	}
	
	//Register Listener
	
	private OnClickListener RegisterListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
			startActivity(intent);
			Username.setText("");
			Password.setText("");
		}
		
		
	};
	
	
	//-----------------------------------------------------------------------------------
	
	//------------------------READ DATA FROM DATABASE------------------------------------
	//Read user for Login Listener
	private boolean read_user(String uname, String pass)
	{
		InputStream is = null;
		String line = null;
		String result = null;
		String url =SaveSharedPreferenceDatabase_data.getURL(MainActivity.this); 
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username",uname));
		nameValuePairs.add(new BasicNameValuePair("password",pass));
	    	
	    try
	    {
			HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost( url + "user.php");
		    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity entity = response.getEntity();
		    is = entity.getContent();
		    Log.e("pass 1", "connection success ");
		}
	    catch(Exception e)
		{
	    	Log.e("Fail 1", e.toString());
		    Toast.makeText(getApplicationContext(), "Invalid IP Address",
			Toast.LENGTH_LONG).show();
		    return false;
		}     
	        
	    try
	    {
	         BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	         StringBuilder sb = new StringBuilder();
	         while ((line = reader.readLine()) != null)
	         {
	       		   sb.append(line + "\n");
	         }
	         is.close();
	         result = sb.toString();
	         Log.e("pass 2", "connection success ");
		}
	    catch(Exception e)
	    {
			Log.e("Fail 2", e.toString());
			return false;
		}     
	       
	   	try
	    {
	        JSONObject json_data = new JSONObject(result);
	        String name=(json_data.getString("username"));
	        amount = (json_data.getString("amount"));
			Toast.makeText(getBaseContext(), "Welcome "+name, Toast.LENGTH_SHORT).show();
			return true;
	    }
	    catch(Exception e)
	    {
	        Log.e("Fail 3", e.toString());
	        return false;
	    }
 
	}
	//-----------------------------------------------------------------------------------
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //set Shared URL link
        SaveSharedPreferenceDatabase_data.setURL(MainActivity.this, url);
        //Read user if connected
        user = SaveSharedPreferenceDatabase_data.getUser(MainActivity.this);
        Username = (EditText) findViewById(R.id.eusername);
    	Password = (EditText) findViewById(R.id.epassword);
    	
    	Register = (Button) findViewById(R.id.bregister);
    	Register.setOnClickListener(RegisterListener);
    	
    	Login = (Button)findViewById(R.id.blogin);
    	Login.setOnClickListener(LoginListener);
        if(user[0].length()> 0 && user[1].length()>0)
        {
        	Username.setText(user[0]);
        	Password.setText(user[1]);
        }
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
