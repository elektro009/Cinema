package com.example.cinema001;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

	public class RegisterActivity extends Activity{
		
		private Button Cancel;
		private OnClickListener CancelListener = new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
			
			
		};
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        
        Cancel = (Button) findViewById(R.id.bcancel);
        Cancel.setOnClickListener(CancelListener);
	}
}
