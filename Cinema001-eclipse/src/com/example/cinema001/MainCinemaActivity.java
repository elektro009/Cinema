package com.example.cinema001;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainCinemaActivity extends Activity {
	
	private Double amount = (double) 0;
	private Double total_pay = (double) 0;
	
	private Button Logout;
	private Button Representation;
	private Button Current_Booking;
	private Button My_Booking;
	private Button Send_Booking;
	private Button Web_Page;
	private Spinner SpinnerCinema;
	private TextView Title;
	private TextView Amount;
	private TextView Pay;
	
	private String[][] cinema;
	private String selected_id_cinema = "";
	

	private String[] user;
	private String[][] representation;
	private int count_booking = 0;
	private String[][] curr_booking;
	private String[][] my_booking;
	private int activated_button = 0;
	
	private ListView MainListView;
	private ArrayAdapter<String> listAdapter;
	
	
	//--------------------------LISTVIEW FUNCTION----------------------------------------
	
	private void setListViewdata(String[][] Data){
		
	    String[] data;
	    data = putDataIn1D(Data);
	    
	    ArrayList<String> dataList = new ArrayList<String>();  
	    dataList.addAll( Arrays.asList(data) ); 
	    
	    listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, dataList);
	    /*for(int i = 0; i < data.length; i++){
	    	
	    	listAdapter.add(data[i]);
	    }*/
	    MainListView.setAdapter(listAdapter);
	}
	// 
	private String[] putDataIn1D(String[][] Data){
		int l = Data.length;
		if(activated_button == 1) l = count_booking;
		String[] data = new String[l];
		for(int i = 0 ; i < l; i++){
			data[i]= "";
			if(activated_button == 0){
				data[i] = "MOVIE: " + Data[i][1] + "\n" + "HALL: " + Data[i][3] + "\n" + "DATE OF REPRESENTATION: " + Data[i][4] + "\n" + "PRICE: " + Data[i][5] + "KM";
			}
			if(activated_button == 1){
				data[i] = "MOVIE: " + Data[i][1] + "\n" + "CINEMA: " + Data[i][2] + "\n" + "HALL: " + Data[i][3] + "\n" + "DATE OF REPRESENTATION: " + Data[i][4] + "\n" + "PRICE: " + Data[i][5] + "KM";
			}
			if(activated_button == 2){
				data[i] = "**BOOKING KEY: " + Data[i][6] + "\n" + "MOVIE: " + Data[i][1] + "\n" + "CINEMA: " + Data[i][2] + "\n" + "HALL: " + Data[i][3] + "\n" + "DATE OF REPRESENTATION: " + Data[i][4] + "\n" + "PRICE: " + Data[i][5] + " KM\n" + "DATE OF BOOKING: " + Data[i][7];
			}
		}
		return data;
		
	}
	
	private OnItemClickListener ListViewClickListener = new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			if(activated_button == 0){
				double difference = amount - total_pay - Double.parseDouble(representation[position][5]);
				if(difference >= 0){
					insertRepresentation(position);
					count_booking++;
					Toast.makeText(getApplicationContext(), "Added Representation " + count_booking +": " + listAdapter.getItem(position),Toast.LENGTH_SHORT).show();
					total_pay = total_pay + Double.parseDouble(representation[position][5]);
					Pay.setText("Total to pay: " + total_pay.toString() + " KM");
					
					
				}
				else
				{
					Toast.makeText(getApplicationContext(), "TOTAL TO PAY > AMOUNT ",Toast.LENGTH_LONG).show();
				}
			}
			if(activated_button == 1){
				
				total_pay = total_pay - Double.parseDouble(curr_booking[position][5]);
				Pay.setText("Total to pay: " + total_pay.toString() + " KM");
				removeRepresentation(position);
				Toast.makeText(getApplicationContext(), "Removed Representation " + (position + 1) + ": " + listAdapter.getItem(position),Toast.LENGTH_LONG).show();
				count_booking--;
				CurrentBookingListener.onClick(MainCinemaActivity.this.getCurrentFocus());
			}
		}
		
	};
	// FOR LISTVIEWCLICKLISTENER
	private void removeRepresentation(int position){
		
		String[][] c = new String[count_booking - 1][curr_booking[0].length];
		for( int i = 0; i < count_booking - 1; i++){
			if(i<position){
				c[i] = curr_booking[i];
			}
			else{
				c[i] = curr_booking[i+1];
			}
		}
		curr_booking = c ;
	}
	// FOR LISTVIEWCLICKLISTENER
	private void insertRepresentation(int position){
		String[][] c = new String[count_booking + 1][representation[0].length];
		for( int i = 0; i < count_booking; i++){
			c[i] = curr_booking[i];
		}
		c[count_booking] = representation[position];
		curr_booking = c ;
	}
	//--------------------------LISTENER-------------------------------------------------
	//Listener for Logout
	private OnClickListener LogoutListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			SaveSharedPreferenceDatabase_data.clearData(MainCinemaActivity.this);
			finish();
		}
		
	};
	
	//Star Main Activity(Login)
	/*private void startMainActivity()
	{
		Intent intent = new Intent(MainCinemaActivity.this, MainActivity.class);
		startActivity(intent);
	}*/
	
	// Current Booking Listener
	private OnClickListener CurrentBookingListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			activated_button = 1;
			Title.setText("CURRENT BOOKING");
			setListViewdata(curr_booking);
		}
		
	};
	
	// My Booking Listener
	private OnClickListener MyBookingListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			
			if(readMyBooking()){
				
				activated_button = 2;
				Title.setText("MY BOOKING");
				setListViewdata(my_booking);
			}
			else{

				Toast.makeText(getApplicationContext(), "My Booking list is empty!", Toast.LENGTH_LONG).show();
				activated_button = 0;
				Title.setText("REPRESENTATIONS");
				setListViewdata(representation);
			}

		}
		
	};
	
	// Web page button listener
	private OnClickListener WebPageListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Uri uri = Uri.parse("https://192.168.147.140/cinema");
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
		}
		
	};
	
	private OnClickListener SendBookingListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(curr_booking.length == 0){
				
				Toast.makeText(getApplicationContext(), "Current Booking list is empty!", Toast.LENGTH_LONG).show();
				return ;
			}
			int i = count_booking;
			amount = amount - total_pay;
			user = SaveSharedPreferenceDatabase_data.getUser(MainCinemaActivity.this);
			user[2] = amount.toString();
			SaveSharedPreferenceDatabase_data.setUser(MainCinemaActivity.this, user[0], user[1], user[2]);
			if(updateAmount() == true){
				
				for(i = count_booking; i > 0; i--){
					
					sendBooking(i-1);
						
				}
				count_booking = i;
				total_pay = (double) 0;
				activated_button = 2;
				Amount.setText("Amount: " + amount + " KM");
				Pay.setText("Total to pay: " + total_pay + " KM");
				readMyBooking();
				setListViewdata(my_booking);
				Title.setText("MY BOOKING");
					
			}
			else{
				

				amount = amount + total_pay;
				user = SaveSharedPreferenceDatabase_data.getUser(MainCinemaActivity.this);
				user[2] = amount.toString();
				SaveSharedPreferenceDatabase_data.setUser(MainCinemaActivity.this, user[0], user[1], user[2]);
			}
		}
		
	};
	private boolean sendBooking(int pos){
		InputStream is = null;
		String line = null;
		String result = null;
		String url =SaveSharedPreferenceDatabase_data.getURL(MainCinemaActivity.this); 
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("id_representation", curr_booking[pos][0]));
		nameValuePairs.add(new BasicNameValuePair("username", SaveSharedPreferenceDatabase_data.getUser(MainCinemaActivity.this)[0]));
	    	
	    try
	    {
			HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost( url + "insert_booking.php");
		    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity entity = response.getEntity();
		    is = entity.getContent();
		    Log.e("pass 1", "connection success ");
		}
	    catch(Exception e)
		{
	    	Log.e("Fail 1", e.toString());
		    Toast.makeText(getApplicationContext(), "Invalid IP Address",
			Toast.LENGTH_LONG).show();
		    return false;
		}     
	        
	    try
	    {
	         BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	         StringBuilder sb = new StringBuilder();
	         while ((line = reader.readLine()) != null)
	         {
	       		   sb.append(line + "\n");
	         }
	         is.close();
	         result = sb.toString();
	         if(result.compareToIgnoreCase("false") == 0){
	        	 
	        	 Toast.makeText(getBaseContext(), "Error with Send Booking! ", Toast.LENGTH_SHORT).show();
	        	 return false;
	         }
	         Log.e("pass 2", "connection success ");
		}
	    catch(Exception e)
	    {
			Log.e("Fail 2", e.toString());
			return false;
		}
	    return true;
		
	}
	private boolean updateAmount(){
		
		InputStream is = null;
		String line = null;
		String result = null;
		String url =SaveSharedPreferenceDatabase_data.getURL(MainCinemaActivity.this);
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("amount", user[2]));
		nameValuePairs.add(new BasicNameValuePair("username", user[0]));
	    	
	    try
	    {
			HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost( url + "update_amount.php");
		    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity entity = response.getEntity();
		    is = entity.getContent();
		    Log.e("pass 1", "connection success ");
		}
	    catch(Exception e)
		{
	    	Log.e("Fail 1", e.toString());
		    Toast.makeText(getApplicationContext(), "Invalid IP Address",
			Toast.LENGTH_LONG).show();
		    return false;
		}     
	        
	    try
	    {
	         BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	         StringBuilder sb = new StringBuilder();
	         while ((line = reader.readLine()) != null)
	         {
	       		   sb.append(line + "\n");
	         }
	         is.close();
	         result = sb.toString();
	         if(result.compareToIgnoreCase("false") == 0){
	        	 
	        	 Toast.makeText(getBaseContext(), "Error with Amount update!", Toast.LENGTH_SHORT).show();
	        	 return false;
	         }
	         Log.e("pass 2", "connection success ");
		}
	    catch(Exception e)
	    {
			Log.e("Fail 2", e.toString());
			return false;
		}
	    return true;
	}
	// Representation Listener
	private OnClickListener RepresentationListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			activated_button = 0;
			readRepresentation();
			Title.setText("REPRESENTATIONS");
			setListViewdata(representation);
		}
		
	};
	
	//add item to spinner cinema
	
	private void addItemSpinnerCinema(){
		
		List<String> list = new ArrayList<String>();
		for(int i = 0; i < cinema.length; i++){
			
			list.add(cinema[i][0] + " - " + cinema[i][1] + " - " + cinema[i][2] + " - " + cinema[i][3]);
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, list);
		//dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		SpinnerCinema.setAdapter(dataAdapter);
	}
	
	//onItemSelected Spinner Cinema Listener
	private OnItemSelectedListener OnItemSelectedCinemaListener = new OnItemSelectedListener(){

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub

			activated_button = 0;
			selected_id_cinema = cinema[position][0];
			readRepresentation();
			Title.setText("REPRESENTATIONS");
			setListViewdata(representation);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
		}
		
	};
	
	//-----------------------------------------------------------------------------------
	
	//----------------------READ DATA FROM DATABASE--------------------------------------
	//read list of cinema from database
	private boolean readCinema()
	{
		InputStream is = null;
		String line = null;
		String result = null;
		String url =SaveSharedPreferenceDatabase_data.getURL(MainCinemaActivity.this); 
		
		//ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		//nameValuePairs.add(new BasicNameValuePair("username",""));
		//nameValuePairs.add(new BasicNameValuePair("password",""));
	    	
	    try
	    {
			HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost( url + "cinema.php");
		    //httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity entity = response.getEntity();
		    is = entity.getContent();
		    Log.e("pass 1", "connection success ");
		}
	    catch(Exception e)
		{
	    	Log.e("Fail 1", e.toString());
		    Toast.makeText(getApplicationContext(), "Invalid IP Address",
			Toast.LENGTH_LONG).show();
		    return false;
		}     
	        
	    try
	    {
	         BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	         StringBuilder sb = new StringBuilder();
	         while ((line = reader.readLine()) != null)
	         {
	       		   sb.append(line + "\n");
	         }
	         is.close();
	         result = sb.toString();
	         Log.e("pass 2", "connection success ");
		}
	    catch(Exception e)
	    {
			Log.e("Fail 2", e.toString());
			return false;
		}

	   	try
	    {
	        JSONArray jArray = new JSONArray(result);
	        cinema = new String[jArray.length()][4];
	        for(int i = 0; i < jArray.length(); i++)
	        {
                JSONObject json_data = jArray.getJSONObject(i);
                cinema[i][0] = json_data.getString("cinema_id");
                cinema[i][1] = json_data.getString("cinema_name");
                cinema[i][2] = json_data.getString("cinema_address");
                cinema[i][3] = json_data.getString("cinema_city_name");
	        }
	        selected_id_cinema = cinema[0][0];
			Toast.makeText(getBaseContext(), "Data loaded! ", Toast.LENGTH_SHORT).show();
			return true;
	    }
	    catch(Exception e)
	    {
	        Log.e("Fail 3", e.toString());
	        return false;
	    }

	}
	
	//read Representation data
	private boolean readRepresentation()
	{
		InputStream is = null;
		String line = null;
		String result = null;
		String url =SaveSharedPreferenceDatabase_data.getURL(MainCinemaActivity.this); 
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("id_cinema",selected_id_cinema));
		//nameValuePairs.add(new BasicNameValuePair("password",""));
	    	
	    try
	    {
			HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost( url + "representation.php");
		    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity entity = response.getEntity();
		    is = entity.getContent();
		    Log.e("pass 1", "connection success ");
		}
	    catch(Exception e)
		{
	    	Log.e("Fail 1", e.toString());
		    Toast.makeText(getApplicationContext(), "Invalid IP Address",
			Toast.LENGTH_LONG).show();
		    return false;
		}     
	        
	    try
	    {
	         BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	         StringBuilder sb = new StringBuilder();
	         while ((line = reader.readLine()) != null)
	         {
	       		   sb.append(line + "\n");
	         }
	         is.close();
	         result = sb.toString();
	         Log.e("pass 2", "connection success ");
		}
	    catch(Exception e)
	    {
			Log.e("Fail 2", e.toString());
			return false;
		}

	   	try
	    {
	        JSONArray jArray = new JSONArray(result);
	        representation = new String[jArray.length()][6];
	        for(int i = 0; i < jArray.length(); i++)
	        {
                JSONObject json_data = jArray.getJSONObject(i);
                representation[i][0] = json_data.getString("representation_id");
                representation[i][1] = json_data.getString("movie_name");
                representation[i][2] = json_data.getString("cinema_id");
                representation[i][3] = json_data.getString("hall_name");
                representation[i][4] = json_data.getString("date_representation");
                representation[i][5] = json_data.getString("price");
	        }
	        //TextView tv = (TextView) findViewById(R.id.textView1);
	        //tv.setText(representation[0][0]);
			Toast.makeText(getBaseContext(), "Data loaded! ", Toast.LENGTH_SHORT).show();
			return true;
	    }
	    catch(Exception e)
	    {
	        Log.e("Fail 3", e.toString());
	        return false;
	    }

	}
	
	//read My Booking data
	private boolean readMyBooking()
	{
		InputStream is = null;
		String line = null;
		String result = null;
		String url = SaveSharedPreferenceDatabase_data.getURL(MainCinemaActivity.this);
		String[] username = SaveSharedPreferenceDatabase_data.getUser(MainCinemaActivity.this);
			
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username",username[0]));
			//nameValuePairs.add(new BasicNameValuePair("password",""));
		    	
		try
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost( url + "booking.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			Log.e("pass 1", "connection success ");
		}
		catch(Exception e)
		{
		    Log.e("Fail 1", e.toString());
			Toast.makeText(getApplicationContext(), "Invalid IP Address",
			Toast.LENGTH_LONG).show();
			return false;
		}     
		        
		try
		{
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		    StringBuilder sb = new StringBuilder();
		    while ((line = reader.readLine()) != null)
		    {
		       	sb.append(line + "\n");
		    }
		    is.close();
		    result = sb.toString();
		    Log.e("pass 2", "connection success ");
		}
		catch(Exception e)
		{
			Log.e("Fail 2", e.toString());
			return false;
		}
		try
		{
		    JSONArray jArray = new JSONArray(result);
		    if(jArray.getString(0).compareTo("empty") == 0) {

				return false;
		    }
		    my_booking = new String[jArray.length()][8];
		    for(int i = 0; i < jArray.length(); i++)
		    {
	             
		    	JSONObject json_data = jArray.getJSONObject(i);
	            my_booking[i][0] = json_data.getString("representation_id");
	            my_booking[i][1] = json_data.getString("movie_name");
	            my_booking[i][2] = json_data.getString("cinema_id");
	            my_booking[i][3] = json_data.getString("hall_name");
	            my_booking[i][4] = json_data.getString("date_representation");
	            my_booking[i][5] = json_data.getString("price");
	            my_booking[i][6] = json_data.getString("booking_key");
	            my_booking[i][7] = json_data.getString("date_booking");
		    }
		        //TextView tv = (TextView) findViewById(R.id.textView1);
		        //tv.setText(representation[0][0]);
			Toast.makeText(getBaseContext(), "Data loaded! ", Toast.LENGTH_SHORT).show();
			return true;
		}
		catch(Exception e)
		{
		    Log.e("Fail 3", e.toString());
		    return false;
		}

	}
	//-----------------------------------------------------------------------------------
	
	
	
	//When login set the user amount
	private void setAmount()
	{
		String[] user = SaveSharedPreferenceDatabase_data.getUser(MainCinemaActivity.this);
		amount = Double.parseDouble(user[2]);
		Amount.setText("Amount: " + amount.toString() + " KM");
	}
	
	//----------------------------------------RUN----------------------------------------
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_cinema);
		//read cinema from database
		readCinema();
		readRepresentation();
		//readMyBooking();
		
		curr_booking = new String[1000][representation[0].length];

		SpinnerCinema = (Spinner) findViewById(R.id.spinnercinema);
		SpinnerCinema.setOnItemSelectedListener(OnItemSelectedCinemaListener);
		addItemSpinnerCinema();
		
		// ListView
		MainListView = (ListView) findViewById(R.id.listView1);
		MainListView.setOnItemClickListener(ListViewClickListener);
		setListViewdata(representation);
		
		//logout button
		Logout = (Button) findViewById(R.id.blogout);
		Logout.setOnClickListener(LogoutListener);
		
		Representation = (Button) findViewById(R.id.brepresentation);
		Representation.setOnClickListener(RepresentationListener);
		
		Current_Booking = (Button) findViewById(R.id.bcurrentbooking);
		Current_Booking.setOnClickListener(CurrentBookingListener);
		
		My_Booking = (Button) findViewById(R.id.bmy_booking);
		My_Booking.setOnClickListener(MyBookingListener);
		
		Send_Booking = (Button) findViewById(R.id.bsendbooking);
		Send_Booking.setOnClickListener(SendBookingListener);
		
		Web_Page = (Button) findViewById(R.id.bwebpage);
		Web_Page.setOnClickListener(WebPageListener);
		
		Title = (TextView) findViewById(R.id.tvtitle);
		Pay = (TextView) findViewById(R.id.tvpay);
		Amount = (TextView) findViewById(R.id.tvamount);
		
		setAmount();
		
	}

	//-----------------------------------------------------------------------------------

}
