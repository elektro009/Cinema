package com.example.cinema001;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class SaveSharedPreferenceDatabase_data {
	
	private static final String[] USER = {"username", "password","amount"};
	private static final String URL = "url";
	//private static final String[][] CINEMA = {{"id", "name", "city", "address"}};
	//private static final String[][] REPRESENTATION = {{"id", "movie", "hall", "full_date_representation", "price"}};
	//private static final String[][] BOOKING = {{"id","cinema","hall","movie","full_date_representation","price","full_date_booking"}};
	
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }
    
    public static void setUser(Context ctx, String username, String password, String amount) 
    {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER[0], username);
        editor.putString(USER[1], password);
        editor.putString(USER[2], amount);
        editor.commit();
    }
    public static void setURL(Context ctx, String url) 
    {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(URL, url);
        editor.commit();
    }
	
    public static String[] getUser(Context ctx)
    {
    	String s[] = new String[3];
    	s[0] = getSharedPreferences(ctx).getString(USER[0], "");
    	s[1] = getSharedPreferences(ctx).getString(USER[1], "");
    	s[2] = getSharedPreferences(ctx).getString(USER[2], "");
    	
        return s;
    }
    public static String getURL(Context ctx)
    {
    	return getSharedPreferences(ctx).getString(URL, "");
    }
    
    public static void clearData(Context ctx) 
    {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.clear(); //clear all stored data
        editor.commit();
    }
    
    
}
